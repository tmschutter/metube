import { useContext } from "react";
import MetubeContext from "../../context/MetubeContext";
import SigninContext from "../../context/SigninContext";
import { API_URL } from "../../App";
function Register() {
  const { setCurrentUser } = useContext(MetubeContext);
  const {
    signinOverlayIsVisible,
    setSigninOverlayIsVisible,
    registering,
    setRegistering,
  } = useContext(SigninContext);
  const handleRegister = async (e) => {
    try {
      e.preventDefault();
      const name = e.target[0].value;
      const username = e.target[1].value;
      const password = e.target[2].value;
      const response = await fetch(`${API_URL}/register`, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ name, username, password }),
      });
      const data = await response.json();
      setCurrentUser(data.user);
      setRegistering(!registering);
      setSigninOverlayIsVisible(!signinOverlayIsVisible);
      e.target.reset();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <form className="signin-form" onSubmit={handleRegister}>
        <div
          className="close-button"
          onClick={() => {
            setSigninOverlayIsVisible(!signinOverlayIsVisible);
          }}
        >
          X
        </div>
        <h2>Register Here!</h2>
        <label>Enter your Name</label>
        <input type="text" placeholder="name" required></input>
        <label>Enter your Username</label>
        <input type="text" placeholder="username" required></input>
        <label>Enter your Password</label>
        <input type="password" placeholder="password" required></input>
        <div className="login-form-buttons-div">
          <button
            className="login-form-button"
            onClick={() => {
              setRegistering(!registering);
            }}
          >
            Sign in instead
          </button>
          <button className="login-form-button">REGISTER</button>
        </div>
      </form>
    </>
  );
}
export default Register;

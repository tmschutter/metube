const app = require("./app");
const cluster = require("cluster");
const dotenv = require("dotenv");
const os = require("os");
dotenv.config();
const PORT = process.env.PORT || 3001;
const numCpu = os.cpus().length;

if (cluster.isMaster) {
  for (let i = 0; i < numCpu; i++) {
    cluster.fork();
  }
  cluster.on("exit", (worker, code, signal) => {
    console.log(`worker ${(worker, process.pid)} died`);
    cluster.fork();
  });
} else {
  app.listen(PORT, () =>
    console.log(`server @ ${process.pid} http://localhost:${PORT}`)
  );
}

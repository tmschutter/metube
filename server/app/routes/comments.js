const express = require("express");
const router = express.Router();
const commentController = require("../controllers/commentController");
router.get("/", commentController.getAllComments);
router.get("/All", commentController.getUserComments);
router.get("/:id", commentController.getOneComment);
router.post("/", commentController.postComment);
router.get("/OnVideo/:video_id", commentController.getCommentsOnVideo);
module.exports = router;

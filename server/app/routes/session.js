const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const pool = require("../../../postgres/db");

router.get("/", async (req, res) => {
  if (!req.cookies.dis_token) {
    return res.json(null);
  }
  try {
    const payload = jwt.verify(
      req.cookies.dis_token,
      process.env.ACCESS_TOKEN_SECRET
    );
    const user_id = payload.user_id;
    const user = await pool.query("SELECT * FROM users WHERE user_id = $1", [
      user_id,
    ]);
    if (user.rows) {
      return res.json(user.rows[0]);
    } else {
      return res.json(null);
    }
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;

const express = require("express");
const router = express.Router();
const pool = require("../../../postgres/db");
const bcrypt = require("bcrypt");
const jwtGenerator = require("../utilities/jwtGenerator");

//login route
router.post("/", async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await pool.query("SELECT * from users WHERE username = $1", [
      username,
    ]);

    //Check if the user exists
    if (user.rows.length < 1) {
      return res.status(404).send({ msg: "User not found" });
    }
    //Compare hashed passwords from client with the one stored in the Database
    const validPassword = await bcrypt.compare(password, user.rows[0].password);
    if (!validPassword) {
      return res.status(401).send({ msg: "Username or password is wrong" });
    }
    //If they pass AUTHENTICATION, create a JSON web token for them
    const tokens = jwtGenerator(user.rows[0]);
    res.cookie("dis_token", tokens.accessToken, {
      httpOnly: true,
      sameSite: "none",
      secure: true,
    });
    res.json({
      user: user.rows[0],
      accessToken: tokens.accessToken,
    });
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;

const pool = require("../../../postgres/db");

async function getAllUsers(req, res) {
  try {
    const { rows } = await pool.query(`SELECT * FROM users`);
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

async function getOneUser(req, res) {
  const { id } = req.params;
  try {
    const { rows } = await pool.query(
      `SELECT * FROM users WHERE user_id = ${id}`
    );
    res.json(rows);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
}

module.exports = { getOneUser, getAllUsers };
